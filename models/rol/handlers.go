package rol

import (
	"fmt"
	"net/http"
	"strconv"

	valid "github.com/asaskevich/govalidator"
	"github.com/labstack/echo"
)

func init() {

}

//CreateHandler crea un rol.
func CreateHandler(c echo.Context) error {
	model := New()

	//Obtenemos el rol enviado en el body.
	err := c.Bind(model)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//Validamos contenido de la estructura
	_, err = valid.ValidateStruct(model)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//Creamos el rol.
	err = model.Create()
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	//Si no ocurrió ningún error retornamos el código http 201 y el nuevo rol.
	return c.JSON(http.StatusCreated, model)

}

//UpdateHandler actualiza un rol.
func UpdateHandler(c echo.Context) error {

	model := New()

	//Obtenemos el rol enviado en el body.
	err := c.Bind(model)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//Validamos contenido de la estructura
	_, err = valid.ValidateStruct(model)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//obtenemos el ID del rol
	id, err := strconv.Atoi(c.Param("rolID"))
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//Aseguramos de que ID > 0
	if id < 1 {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//Establecemos el ID
	model.ID = id

	//Actualizamos el rol.
	err = model.Update()
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	//Si no ocurrió ningún error retornamos el código http 204.
	return c.NoContent(http.StatusNoContent)

}

//DeleteHandler elimina un rol.
func DeleteHandler(c echo.Context) error {

	//Creamos un nuevo modelo de la entidad Rol.
	model := New()

	//Obtenemos el ID enviado como parámetro.
	id, err := strconv.Atoi(c.Param("rolID"))
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//Comprobamos que ID > 0
	if id < 1 {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Errorf("el ID %d no es valido, el ID debe "+
			"ser mayor que 0", id))
	}

	//Establecemos el ID del rol
	model.ID = id

	//Eliminamos el rol
	err = model.Delete()
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	//Si no ocurrió ningún error retornamos el código http 204.
	return c.NoContent(http.StatusNoContent)
}

//GetAllHandler obtiene un listado de roles.
func GetAllHandler(c echo.Context) error {

	//Creamos un nuevo modelo de la entidad Rol.
	model := New()

	//Obtenemos el limite enviado como parámetro.
	limit, err := strconv.Atoi(c.QueryParam("limit"))
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//Obtenemos la página enviada como parámetro.
	page, err := strconv.Atoi(c.QueryParam("page"))
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//Obtenemos el listado de roles.
	models, err := model.GetAll(limit, page)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	//Si no ocurrió ningún error retornamos el código http 200 y el listado de roles.
	return c.JSON(http.StatusOK, models)
}

//GetByIDHandler obtiene un rol por su ID.
func GetByIDHandler(c echo.Context) error {

	//Creamos un nuevo modelo de la entidad Rol.
	model := New()

	//Obtenemos el ID enviado como parámetro.
	id, err := strconv.Atoi(c.Param("rolID"))
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//Comprobamos que ID > 0
	if id < 1 {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Errorf("el ID %d no es valido, el ID debe "+
			"ser mayor que 0", id))
	}

	//Establecemos el ID del rol
	model.ID = id

	//Obtenemos el rol.
	err = model.GetByID()
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	//Si no ocurrió ningún error retornamos el código http 200 y el rol.
	return c.JSON(http.StatusOK, model)
}

//GetByNameHandler obtiene un rol por su nombre.
func GetByNameHandler(c echo.Context) error {

	//Creamos un nuevo modelo de la entidad Rol.
	model := New()

	//Obtenemos el nombre enviado como parámetro.
	name := c.Param("rolName")

	//Establecemos el nombre del rol
	model.Name = name

	//Obtenemos el rol.
	err := model.GetByName()
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	//Si no ocurrió ningún error retornamos el código http 200 y el rol.
	return c.JSON(http.StatusOK, model)
}
