package rol

import (
	"time"
)

//Datastore es la interfaz que debe implementar el encargado de suplir la conexión a la Base de Datos.
type Datastore interface {
	Create(m *Model) error
	Update(m *Model) error
	Delete(m *Model) error
	GetAll(limit, page int) (*Models, error)
	GetByID(m *Model) error
	GetByName(m *Model) error
}

var ds Datastore

//SetDatastore es el método utilizado para establecer la estructura que va a implementar la interfaz Datastore.
func SetDatastore(d Datastore) {
	ds = d
}

//Model es la estructura que representa la entidad Rol.
type Model struct {
	ID        int        `json:"id" gorm:"primary_key,index;not null;unique" valid:"-"`
	Name      string     `json:"name" gorm:"index;not null;unique" valid:"alphanum,required"`
	Active    bool       `json:"active" valid:"-"`
	CreateAt  time.Time  `json:"create_at,omitempty" valid:"-"`
	UpdateAt  time.Time  `json:"update_at,omitempty" valid:"-"`
	DeletedAt *time.Time `json:"-" sql:"index" valid:"-"`
}

//New retorna un puntero a un nuevo modelo de la entidad Rol.
func New() *Model {
	return &Model{}
}

//Create crea un nuevo Rol.
func (m *Model) Create() error {
	return ds.Create(m)
}

//Update actualiza un Rol.
func (m *Model) Update() error {
	return ds.Update(m)
}

//Delete elimina un Rol.
func (m *Model) Delete() error {
	return ds.Delete(m)
}

//GetAll obtiene un listado de Roles, recibe como parámetro la cantidad de
//registros que quieren que retorne "limit" y la página "page" de donde se va a empezar a buscar.
func (m *Model) GetAll(limit, page int) (*Models, error) {
	return ds.GetAll(limit, page)
}

//GetByID obtiene un Rol por su ID.
func (m *Model) GetByID() error {
	return ds.GetByID(m)
}

//GetByName obtiene un Rol por su nombre.
func (m *Model) GetByName() error {
	return ds.GetByName(m)
}

//TableName es utilizado para especificar el nombre que se debe utilizar en la
//Base de Datos para guardar esta entidad de datos.
//Nota: este método fue creado para suplir una necesidad del ORM GORM, por lo que
//si se deja de utilizar este ORM este método puede ser eliminado.
func (m *Model) TableName() string {
	return "roles"
}

//Models es un slice de modelos de la entidad Rol.
type Models []Model

//NewSlice retorna un puntero de []Model.
func NewSlice() *Models {
	return &Models{}
}
