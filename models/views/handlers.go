package views

import (
	"net/http"

	"github.com/labstack/echo"
)

//HomeHandler maneja el home.
func HomeHandler(c echo.Context) error {

	return c.Render(http.StatusOK, "home", struct {
		Name string
	}{"Golang-es"})

}
