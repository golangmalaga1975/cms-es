# Videoquest

## Requisitos 

- Go 1.9 o superior.


 
##Pasos para correr el Sistema


**1. Instalar Go Dep para gestionar las dependencias**

`$ go get -u github.com/golang/dep/cmd/dep`

**2. Ejecutar el comando:**

`$ dep ensure`

**3. Compilar el programa:**

`$ go build`
**4. Ejecutar el programa:**

- Linux

    `./cms-es`

. Windows

     `cms-es.exe`



