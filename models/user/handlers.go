package user

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/asaskevich/govalidator"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"gitlab.com/golanges/cms-es/configuration"
)

//CreateHandler crea un usuario.
func CreateHandler(c echo.Context) error {

	//Creamos un nuevo modelo de la entidad User.
	model := New()

	//Obtenemos el usuario enviado en el body.

	err := c.Bind(model)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}
	//Validamos la estructura
	err = c.Validate(model)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}
	//Creamos el usuario.
	err = model.Create()
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	//Si no ocurrió ningún error retornamos el código http 201 y el nuevo usuario.
	return c.JSON(http.StatusCreated, model)

}

//UpdateHandler actualiza un usuario.
func UpdateHandler(c echo.Context) error {

	//Creamos un nuevo modelo de la entidad User.
	model := New()

	//Obtenemos el usuario enviado en el body.
	err := c.Bind(model)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//Validamos la estructura
	err = c.Validate(model)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//obtenemos el ID del usuario
	id, err := strconv.Atoi(c.Param("userID"))
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//Aseguramos de que ID > 0
	if id < 1 {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//Establecemos el ID
	model.ID = id

	//Actualizamos el usuario.
	err = model.Update()
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	//Si no ocurrió ningún error retornamos el código http 204.
	return c.NoContent(http.StatusNoContent)

}

//DeleteHandler elimina un usuario.
func DeleteHandler(c echo.Context) error {

	//Creamos un nuevo modelo de la entidad User.
	model := New()

	//Obtenemos el ID enviado como parámetro.
	id, err := strconv.Atoi(c.Param("userID"))
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}
	//Comprobamos que ID > 0
	if id < 1 {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Errorf("el ID %d no es valido, el ID debe "+
			"ser mayor que 0", id))
	}

	//Establecemos el ID del usuario
	model.ID = id

	//Eliminamos el usuario
	err = model.Delete()
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	//Si no ocurrió ningún
	// error retornamos el código http 204.
	return c.NoContent(http.StatusNoContent)
}

//GetAllHandler obtiene un listado de usuarios.
func GetAllHandler(c echo.Context) error {

	//Creamos un nuevo modelo de la entidad User.
	model := New()

	//Obtenemos el limite enviado como parámetro.
	limit, err := strconv.Atoi(c.QueryParam("limit"))
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//Obtenemos la página enviada como parámetro.
	page, err := strconv.Atoi(c.QueryParam("page"))
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//Obtenemos el listado de usuarios
	models, err := model.GetAll(limit, page)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	//Si no ocurrió ningún error retornamos el código http 200 y el listado de usuarios.
	return c.JSON(http.StatusOK, models)
}

//GetByIDHandler obtiene un usuario por su ID.
func GetByIDHandler(c echo.Context) error {

	//Creamos un nuevo modelo de la entidad User.
	model := New()

	//Obtenemos el ID enviado como parámetro.
	id, err := strconv.Atoi(c.Param("userID"))
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//Comprobamos que ID > 0
	if id < 1 {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Errorf("el ID %d no es valido, el ID debe"+
			" ser mayor que 0", id))
	}

	//Establecemos el ID del usuario
	model.ID = id

	//Obtenemos el usuario.
	err = model.GetByID()
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	//Si no ocurrió ningún error retornamos el código http 200 y el usuario.
	return c.JSON(http.StatusOK, model)
}

//GetByEmailHandler obtiene un usuario por su email.
func GetByEmailHandler(c echo.Context) error {

	//Creamos un nuevo modelo de la entidad User.
	model := New()

	//Obtenemos y establecemos el email enviado como parámetro.
	model.Email = c.Param("email")

	//Validamos el email
	ok := govalidator.IsEmail(model.Email)
	if !ok {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Errorf("el Email %s no es valido",
			model.Email))
	}

	//Obtenemos el usuario.
	err := model.GetByEmail()
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	//Si no ocurrió ningún error retornamos el código http 200 y el usuario.
	return c.JSON(http.StatusOK, model)
}

//GetByUserNameHandler obtiene un usuario por su userName.
func GetByUserNameHandler(c echo.Context) error {

	//Creamos un nuevo modelo de la entidad User.
	model := New()

	//Obtenemos y establecemos el username enviado como parámetro.
	model.UserName = c.Param("userName")

	//Verificamos que es valido
	ok := govalidator.IsAlphanumeric(model.UserName)

	if len(model.UserName) < 6 && !ok {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Errorf("el UserName %s no es valido",
			model.UserName))
	}

	//Obtenemos el usuario.
	err := model.GetByUserName()
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	//Si no ocurrió ningún error retornamos el código http 200 y el usuario.
	return c.JSON(http.StatusOK, model)
}

//ChangePasswordHandler actualiza el password de un usuario.
func ChangePasswordHandler(c echo.Context) error {

	//Creamos un nuevo modelo de la entidad User.
	model := New()

	//Creamos una nueva estructura para el binding
	p := &struct {
		Password string `json:"password"`
	}{}

	//Obtenemos el password enviado en el body.
	err := c.Bind(p)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//Verificamos que es valido
	if len(p.Password) < 6 {
		return echo.NewHTTPError(http.StatusBadRequest, errors.New("el password debe tener por lo menos "+
			"6 caracteres valido"))
	}

	//Obtenemos el ID enviado como parámetro.
	id, err := strconv.Atoi(c.Param("userID"))
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//Comprobamos que ID > 0
	if id < 1 {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Errorf("el ID %d no es valido, el ID debe "+
			"ser mayor que 0", id))
	}

	//Establecemos el ID del usuario
	model.ID = id

	//Establecemos el nuevo password enviado como parámetro.
	model.Password = p.Password

	//Cambiamos el password
	err = model.ChangePassword()
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	//Si no ocurrió ningún error retornamos el código http 204
	return c.NoContent(http.StatusNoContent)
}

//PublicLoginHandler procesa el login del usuario.
func PublicLoginHandler(c echo.Context) error {

	//Creamos una nueva estructura para el binding
	auth := &struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}{}

	//Obtenemos las credenciales
	err := c.Bind(auth)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	//Verificamos que ni el email ni el password estén vacíos
	if auth.Email == "" || len(auth.Password) < 6 {
		return echo.NewHTTPError(http.StatusBadRequest, errors.New("el email o el password no son validos"))
	}
	//Validamos el email
	ok := govalidator.IsEmail(auth.Email)
	if !ok {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Errorf("el Email %s no es valido",
			auth.Email))
	}

	//Creamos un nuevo modelo de la entidad User.
	m := New()

	//Establecemos el email
	m.Email = auth.Email

	//Establecemos el password
	m.Password = auth.Password

	//Realizamos el Login
	err = m.Login()
	if err != nil {
		return echo.ErrUnauthorized
	}

	//Creamos el Token
	token := jwt.New(jwt.SigningMethodHS256)

	//Establecemos los claims
	claims := token.Claims.(jwt.MapClaims)
	claims["userID"] = m.ID
	claims["exp"] = time.Now().Add(time.Hour * time.Duration(configuration.AppConfig.TokenExpTime)).Unix()

	//Generamos el token codificado
	t, err := token.SignedString([]byte(configuration.AppConfig.TokenKey))
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	//Retornamos el token y el usuario
	return c.JSON(http.StatusOK, struct {
		Token string `json:"token"`
		User  Model  `json:"user"`
	}{t, *m})

}
