package user

import "github.com/labstack/echo"

//GetRoutes establece los endpoints de del recurso User.
func GetRoutes(g *echo.Group) {
	g.POST("/users", CreateHandler)
	g.PUT("/users/:userID", UpdateHandler)
	g.DELETE("/users/:userID", DeleteHandler)
	g.GET("/users", GetAllHandler)
	g.GET("/users/:userID", GetByIDHandler)
	g.GET("/users/email/:email", GetByEmailHandler)
	g.GET("/users/username/:userName", GetByUserNameHandler)
	g.PUT("/users/changepw/:userID", ChangePasswordHandler)

}
