package router

import (
	"fmt"
	"html/template"
	"io"
	"net/http"
	"os"
	"path"

	"github.com/asaskevich/govalidator"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/golanges/cms-es/configuration"
	"gitlab.com/golanges/cms-es/models/rol"
	"gitlab.com/golanges/cms-es/models/user"
	"gitlab.com/golanges/cms-es/models/views"
	"gopkg.in/mgo.v2/bson"
)

//CustomValidator es una estructura vacía para implementar la interfaz Validate requerida por echo.
type CustomValidator struct {
}

//Validate implementación de la interfaz Validate de echo.
func (cv CustomValidator) Validate(i interface{}) error {
	_, err := govalidator.ValidateStruct(i)

	return err
}

//TemplateEngine es la estructura que implementa la interfaz Render de echo
type TemplateEngine struct {
	templates *template.Template
}

func (t *TemplateEngine) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

//InitRouter inicializa y retorna el router.
func InitRouter() *echo.Echo {

	//Creamos una nueva instancia de echo
	e := echo.New()

	/***
	 *       _____             __ _                 _   _
	 *      / ____|           / _(_)               | | (_)
	 *     | |     ___  _ __ | |_ _  __ _ _ __ __ _| |_ _  ___  _ __  ___
	 *     | |    / _ \| '_ \|  _| |/ _` | '__/ _` | __| |/ _ \| '_ \/ __|
	 *     | |___| (_) | | | | | | | (_| | | | (_| | |_| | (_) | | | \__ \
	 *      \_____\___/|_| |_|_| |_|\__, |_|  \__,_|\__|_|\___/|_| |_|___/
	 *                               __/ |
	 *                              |___/
	 */

	//Configuramos el validador por defecto.
	e.Validator = &CustomValidator{}

	//Configuramos el template engine por defecto.

	e.Renderer = &TemplateEngine{
		templates: template.Must(template.ParseGlob("templates/*.gohtml")),
	}

	/***
	 *      __  __ _     _     _ _
	 *     |  \/  (_)   | |   | | |
	 *     | \  / |_  __| | __| | | _____      ____ _ _ __ ___  ___
	 *     | |\/| | |/ _` |/ _` | |/ _ \ \ /\ / / _` | '__/ _ \/ __|
	 *     | |  | | | (_| | (_| | |  __/\ V  V / (_| | | |  __/\__ \
	 *     |_|  |_|_|\__,_|\__,_|_|\___| \_/\_/ \__,_|_|  \___||___/
	 *
	 *
	 */

	e.Use(middleware.Recover())
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Output: getLogger(),
	}))
	e.Use(middleware.CORS())

	/***
	 *      _____       _     _ _        _____             _
	 *     |  __ \     | |   | (_)      |  __ \           | |
	 *     | |__) |   _| |__ | |_  ___  | |__) |___  _   _| |_ ___  ___
	 *     |  ___/ | | | '_ \| | |/ __| |  _  // _ \| | | | __/ _ \/ __|
	 *     | |   | |_| | |_) | | | (__  | | \ \ (_) | |_| | ||  __/\__ \
	 *     |_|    \__,_|_.__/|_|_|\___| |_|  \_\___/ \__,_|\__\___||___/
	 *
	 *
	 */

	//login
	e.POST("/login", user.PublicLoginHandler)

	//Sirviendo los archivos estáticos
	e.Static("/", "public")

	//Views
	e.GET("/", views.HomeHandler)

	/***
	 *               _____ _____   _____       _
	 *         /\   |  __ \_   _| |  __ \     | |
	 *        /  \  | |__) || |   | |__) |   _| |_ ___  ___
	 *       / /\ \ |  ___/ | |   |  _  / | | | __/ _ \/ __|
	 *      / ____ \| |    _| |_  | | \ \ |_| | ||  __/\__ \
	 *     /_/    \_\_|   |_____| |_|  \_\__,_|\__\___||___/
	 *
	 *
	 */

	api := e.Group("/api", middleware.JWT([]byte(configuration.AppConfig.TokenKey)))

	//User
	user.GetRoutes(api)

	//Rol
	rol.GetRoutes(api)

	//UploadImage
	api.POST("/upload", uploadImageHandler)

	return e
}

func getLogger() io.Writer {
	f, err := os.OpenFile("logfile.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		return os.Stdout
	}
	return f
}

//uploadImageHandler handler para manejar las subidas de imágenes al servidor.
func uploadImageHandler(c echo.Context) error {
	//TODO Verificar que son imágenes validas
	//Obtenemos el archivo desde el formulario
	file, err := c.FormFile("image")
	if err != nil {
		return err
	}
	//Obtenemos el Header del archivo
	src, err := file.Open()
	if err != nil {
		return err
	}
	defer src.Close()

	//Creamos el nombre del archivo concatenando un nuevo ID BSON y la extension del archivo.
	fileName := bson.NewObjectId().Hex() + path.Ext(file.Filename)
	fileRoute := path.Join("public", "upload", "images", fileName)

	//Creamos el archivo de destino
	dst, err := os.Create(fileRoute)
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer dst.Close()

	//Copiamos el archivo que hemos recibido en el archivo de destino que hemos creado.
	if _, err = io.Copy(dst, src); err != nil {
		return err
	}

	//Creamos nuestra respuesta.
	resp := struct {
		ImageName string
		ImageURL  string
	}{
		ImageName: fileName,
		ImageURL:  fmt.Sprintf("%s://%s/upload/images/%s", c.Scheme(), c.Request().Host, fileName),
	}

	//Retornamos nuestra respuesta
	return c.JSON(http.StatusOK, resp)

}
