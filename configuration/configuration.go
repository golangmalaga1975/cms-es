package configuration

import (
	"encoding/json"
	"log"
	"os"
)

//ConfigFilePath Ruta del archivo del archivo config.json.
var ConfigFilePath string

type Configuration struct {
	Server         string
	Development    bool
	DBEngine       string
	DBHost         string
	DBName         string
	DBUser         string
	DBPassword     string
	TestDBHost     string
	TestDBName     string
	TestDBUser     string
	TestDBPassword string
	TokenKey       string
	TokenExpTime   int
}

func (c *Configuration) ChangeToTest() {
	AppConfig.DBHost = AppConfig.TestDBHost
	AppConfig.DBName = AppConfig.TestDBName
	AppConfig.DBPassword = AppConfig.TestDBPassword
	AppConfig.DBUser = AppConfig.TestDBUser
}
func init() {
	ConfigFilePath = "./config.json"
}

//AppConfig Almacena la configuración del archivo config.json.
var AppConfig Configuration

//LoadAppConfig Lee el archivo config.json y lo decodifica en AppConfig
func LoadAppConfig() {
	file, err := os.Open(ConfigFilePath)
	defer file.Close()
	if err != nil {
		log.Fatal(err)
	}
	decoder := json.NewDecoder(file)
	AppConfig = Configuration{}
	err = decoder.Decode(&AppConfig)
	if err != nil {
		log.Fatal(err)
	}

}
