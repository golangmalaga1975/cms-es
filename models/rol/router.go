package rol

import "github.com/labstack/echo"

//GetRoutes establece los endpoints de del recurso Rol.
func GetRoutes(g *echo.Group) {
	g.POST("/roles", CreateHandler)
	g.PUT("/roles/:rolID", UpdateHandler)
	g.DELETE("/roles/:rolID", DeleteHandler)
	g.GET("/roles", GetAllHandler)
	g.GET("/roles/:rolID", GetByIDHandler)
	g.GET("/roles/name/:rolName", GetByNameHandler)

}
