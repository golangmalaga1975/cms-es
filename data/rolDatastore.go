package data

import (
	"time"

	"gitlab.com/golanges/cms-es/models/rol"
)

//RolDatastore es la estructura que implementa la interfaz Datastore del paquete rol.
type RolDatastore struct {
}

//Create crea un nuevo Rol.
func (d RolDatastore) Create(m *rol.Model) error {

	//Establecemos la hora de creación y actualización
	m.CreateAt = time.Now()
	m.UpdateAt = time.Now()

	//Creamos el rol
	return GetDB().Create(m).Error

}

//Update actualiza un Rol.
func (d RolDatastore) Update(m *rol.Model) error {

	//Establecemos la hora de actualización
	m.UpdateAt = time.Now()

	//Actualizamos el rol
	return GetDB().Save(m).Error

}

//Delete elimina un Rol.
func (d RolDatastore) Delete(m *rol.Model) error {

	//Eliminamos el rol
	return GetDB().Delete(m).Error

}

//GetAll obtiene un listado de Roles, recibe como parámetro la cantidad de
//registros que quieren que retorne "limit" y la página "page" de donde se va a empezar a buscar.
func (d RolDatastore) GetAll(limit, page int) (*rol.Models, error) {

	//Aseguramos que la página mínima sea >= 1
	if page < 1 {
		page = 1
	}

	//Creamos un puntero a un nuevo slice de Roles.
	roles := rol.NewSlice()

	//Establecemos desde donde se va ha empezar a listar.
	offset := (page - 1) * limit

	switch {
	case limit > 0:

		//Obtenemos los roles especificando un limite
		err := GetDB().Offset(offset).Limit(limit).Find(&roles).Error
		if err != nil {
			return nil, err
		}
	case limit < 1:

		//Obtenemos todos los roles si especificar un limite
		err := GetDB().Offset(offset).Find(&roles).Error
		if err != nil {

			return nil, err
		}

	}

	//Retornamos los roles
	return roles, nil
}

//GetByID obtiene un Rol por su ID.
func (d RolDatastore) GetByID(m *rol.Model) error {

	//Obtenemos el rol
	return GetDB().First(m, m.ID).Error

}

//GetByName obtiene un Rol por su nombre.
func (d RolDatastore) GetByName(m *rol.Model) error {

	//Obtenemos el rol
	return GetDB().Where("name = ?", m.Name).First(m).Error

}
