package user

import (
	"time"

	"gitlab.com/golanges/cms-es/models/rol"
)

//Datastore es la interfaz que debe implementar el encargado de suplir la conexión a la Base de Datos.
type Datastore interface {
	Create(m *Model) error
	Update(m *Model) error
	Delete(m *Model) error
	GetAll(limit, page int) (*Models, error)
	GetByID(m *Model) error
	GetByUserName(m *Model) error
	GetByEmail(m *Model) error
	ChangePassword(m *Model) error
	Login(m *Model) error
}

var storage Datastore

//SetDatastore es el método utilizado para establecer la estructura que va a implementar la interfaz Datastore.
func SetDatastore(s Datastore) {
	storage = s
}

//Model es la estructura que representa la entidad User.
type Model struct {
	ID           int        `json:"id" gorm:"primary_key,index;not null;unique" valid:"-"`
	UserName     string     `json:"user_name" gorm:"not null;unique" valid:"alphanum,required"`
	FullName     string     `json:"full_name" valid:"ascii,required"`
	Email        string     `json:"email" gorm:"index;not null;unique" valid:"email,required"`
	Password     string     `json:"password,omitempty" gorm:"-" valid:"length(6|32),optional"`
	HashPassword []byte     `json:"-" gorm:"not null" valid:"-"`
	Rol          rol.Model  `json:"rol,omitempty" valid:"-"`
	RolID        int        `json:"rol_id," gorm:"not null" valid:"required"`
	ImageURL     string     `json:"image" valid:"url,optional"`
	CreateAt     time.Time  `json:"create_at,omitempty" valid:"-"`
	UpdateAt     time.Time  `json:"update_at,omitempty" valid:"-"`
	DeletedAt    *time.Time `json:"-" sql:"index" valid:"-"`
}

//New retorna un puntero a un nuevo modelo de la entidad User.
func New() *Model {
	return &Model{}
}

//Create crea un nuevo user.
func (m *Model) Create() error {
	return storage.Create(m)
}

//Update actualiza un user.
func (m *Model) Update() error {
	return storage.Update(m)
}

//Delete elimina un user.
func (m *Model) Delete() error {
	return storage.Delete(m)
}

//GetAll obtiene un listado de Users, recibe como parámetro la cantidad de
//registros que quieren que retorne "limit" y la página "page" de donde se va a empezar a buscar.
func (m *Model) GetAll(limit, page int) (*Models, error) {
	return storage.GetAll(limit, page)
}

//GetByID obtiene un user por su ID.
func (m *Model) GetByID() error {
	return storage.GetByID(m)
}

//GetByUserName obtiene un user por su userName.
func (m *Model) GetByUserName() error {
	return storage.GetByUserName(m)
}

//GetByEmail obtiene un user por su email.
func (m *Model) GetByEmail() error {
	return storage.GetByEmail(m)
}

//TableName es utilizado para especificar el nombre que se debe utilizar en la
//Base de Datos para guardar esta entidad de datos.
//Nota: este método fue creado para suplir una necesidad del ORM GORM, por lo que
//si se deja de utilizar este ORM este método puede ser eliminado.
func (m *Model) TableName() string {
	return "users"
}

//ChangePassword actualiza el password de un user.
func (m *Model) ChangePassword() error {
	return storage.ChangePassword(m)
}

//Login realiza el login de un usuario.
func (m *Model) Login() error {
	return storage.Login(m)
}

//Models es un slice de modelos de la entidad User.
type Models []Model

//NewSlice retorna un puntero de []Model.
func NewSlice() *Models {
	return &Models{}
}
