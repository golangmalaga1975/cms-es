package data

import (
	"errors"
	"fmt"
	"time"

	"gitlab.com/golanges/cms-es/models/rol"
	"gitlab.com/golanges/cms-es/models/user"

	"golang.org/x/crypto/bcrypt"
)

//UserDatastore es la estructura que implementa la interfaz Datastore del paquete user.
type UserDatastore struct {
}

//Create crea un nuevo usuario.
func (userStorage UserDatastore) Create(m *user.Model) error {

	//Encriptamos el password del usuario
	hpass, err := bcrypt.GenerateFromPassword([]byte(m.Password), bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}

	//Establecemos el hash
	m.HashPassword = hpass

	//Limpiamos el password enviado por el usuario
	m.Password = ""

	//Establecemos la fecha de creación y actualización
	m.CreateAt = time.Now()
	m.UpdateAt = time.Now()

	//Creamos el usuario
	err = GetDB().Create(m).Error
	if err != nil {
		return err
	}

	//Eliminamos el Hash que habíamos creado en el struct user
	m.HashPassword = []byte{}

	//Completamos el campo Rol a partir del ID del campo RolID que se guarda en la base de datos.
	//Se realiza este paso para poder devolver la estructura user con todos los datos y no halla
	// realizar otra petición a la api para obtener los datos del rol.

	//TODO: Refactorizar este codigo ya que se utiliza en multiples sitios.

	//Creamos un nuevo modelo de la entidad Rol.
	r := rol.New()

	//Establecemos el ID.
	r.ID = m.RolID

	//Obtenemos el Rol.
	err = r.GetByID()
	if err != nil {
		return errors.New("error al obtener el rol del usuario")
	}

	//Establecemos el Rol
	m.Rol = *r

	return nil
}

//Update actualiza un usuario.
func (userStorage UserDatastore) Update(m *user.Model) error {

	//Nos aseguramos de que el objeto no tenga password ni hash
	m.Password = ""
	m.HashPassword = []byte{}

	//Establecemos la fecha de actualización
	m.UpdateAt = time.Now()

	//Actualizamos el usuario
	return GetDB().Model(m).Updates(m).Error

}

//Delete elimina un usuario.
func (userStorage UserDatastore) Delete(m *user.Model) error {

	//Eliminamos el usuario
	return GetDB().Delete(m).Error

}

//GetAll obtiene un listado de usuarios, recibe como parámetro la cantidad de
// registros que quieren que retorne "limit" y la página "page" de donde se va a empezar a buscar.
func (userStorage UserDatastore) GetAll(limit, page int) (*user.Models, error) {

	//Aseguramos que la página mínima sea 1
	if page < 1 {
		page = 1
	}

	//Creamos un puntero a un nuevo slice de usuarios.
	users := user.NewSlice()

	switch {
	case limit > 0:

		//Obtenemos los usuario especificando un limite
		err := GetDB().Offset((page - 1) * limit).Limit(limit).Find(users).Error
		if err != nil {

			return nil, err
		}
	case limit < 1:

		//Obtenemos todos los usuario
		err := GetDB().Offset((page - 1) * limit).Find(users).Error
		if err != nil {

			return nil, err
		}

	}
	//TODO: Establecer el rol de cada usuario al igual como se hace cuando se crea un nuevo usuario,
	// de lo contrario se tendrá qye realizar una llamad a ala API por cada uno de los usuarios, para,
	// obtener los datos del Rol..

	//Retornamos los usuarios
	return users, nil
}

//GetByID obtiene un usuario por su ID.
func (userStorage UserDatastore) GetByID(m *user.Model) error {

	//Obtenemos el usuario
	return GetDB().Where("id = ?", m.ID).First(m).Error

}

//GetByUserName obtiene un usuario por su username.
func (userStorage UserDatastore) GetByUserName(m *user.Model) error {

	//Obtenemos el usuario
	return GetDB().Where("user_name = ?", m.UserName).First(m).Error
}

//GetByEmail obtiene un usuario por su email.
func (userStorage UserDatastore) GetByEmail(m *user.Model) error {

	//Obtenemos el usuario
	return GetDB().Where("email = ?", m.Email).First(m).Error
}

//Login verifica en la base de datos que las credenciales del usuario
// son correctas y de ser asi retorna el usuario.
func (userStorage UserDatastore) Login(m *user.Model) error {

	//Hacemos una copia del usuario
	u := *m

	//Obtenemos el usuario por el email
	if ok := GetDB().Where("email = ?", m.Email).First(m).RecordNotFound(); ok {
		return fmt.Errorf("el email %s no existe", m.Email)
	}

	//Validamos el Password
	err := bcrypt.CompareHashAndPassword(m.HashPassword, []byte(u.Password))
	if err != nil {
		m = &u
		return errors.New("password incorrecto")
	}

	//Completamos el campo Rol a partir del ID del campo RolID que se guarda en la base de datos.
	//Se realiza este paso para poder devolver la estructura user con todos los datos y no halla
	// realizar otra petición a la api para obtener los datos del rol.

	//Creamos un nuevo modelo de la entidad Rol.
	r := rol.New()

	//Establecemos el ID.
	r.ID = m.RolID

	//Obtenemos el Rol.
	err = r.GetByID()
	if err != nil {
		return errors.New("error al obtener el rol del usuario")
	}

	//Establecemos el Rol
	m.Rol = *r

	//Eliminamos el password y el hash del objeto usuario
	m.HashPassword = []byte{}
	m.Password = ""

	return nil

}

//ChangePassword actualiza el password de un usuario.
func (userStorage UserDatastore) ChangePassword(m *user.Model) error {

	//Encriptamos el nuevo password
	hpass, err := bcrypt.GenerateFromPassword([]byte(m.Password), bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}

	//Actualizamos el usuario
	err = GetDB().Model(m).Update("hash_password", hpass).Error
	if err != nil {
		return err
	}

	//Eliminamos el password y el hash del objeto usuario
	m.Password = ""
	m.HashPassword = []byte{}

	return nil

}
