package postgres

import (
	"fmt"
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

//Connect crea y retorna una conexión a la base de datos postgres.
func Connect(host, user, password, dbName string) *gorm.DB {

	//Creamos la cadena de conexión
	cs := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s",
		host,
		user,
		dbName,
		password)

	//Nos conectamos con la base de datos.
	db, err := gorm.Open("postgres", cs)
	if err != nil {
		log.Fatal(err)
	}

	//Retornamos la conexión.
	return db
}
