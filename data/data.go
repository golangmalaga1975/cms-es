package data

import (
	"log"

	"gitlab.com/golanges/cms-es/configuration"
	"gitlab.com/golanges/cms-es/data/postgres"
	"gitlab.com/golanges/cms-es/models/rol"
	"gitlab.com/golanges/cms-es/models/user"

	"github.com/jinzhu/gorm"
)

var dataBase *gorm.DB

//GetDB retorna un puntero de la conexión  a la base de datos.
func GetDB() *gorm.DB {

	return dataBase

}

//Close cierra la conexión con la base de datos.
func Close() {
	dataBase.Close()
}

//InitDB inicializa el paquete data
func InitDB(c configuration.Configuration) {

	//Dependiendo el engine especificado obtenemos la conexión a la base de datos.
	//Nota: es recomendable implementar las conexiones a las bases de datos mas populares
	//como mySql, SQLServer, etc.
	switch c.DBEngine {
	case "postgres":

		dataBase = postgres.Connect(
			configuration.AppConfig.DBHost,
			configuration.AppConfig.DBUser,
			configuration.AppConfig.DBPassword,
			configuration.AppConfig.DBName,
		)

	default:
		log.Fatalf("%s no es un Engine soportado", configuration.AppConfig.DBEngine)
	}

	//Desactivamos la pluralización de los nombres de las tablas.
	dataBase.SingularTable(true)

	//Establecemos la cantidad máxima de conexiones inactivas.
	dataBase.DB().SetMaxIdleConns(50)

	//Establecemos la cantidad máxima de conexiones abiertas.
	dataBase.DB().SetMaxOpenConns(50)

	//Si estamos en el entorno de desarrollo activamos el modo  Log para
	//que se impriman en consola las consultas SQL que hace el ORM.
	if c.Development {
		dataBase.LogMode(true)
	}

	//Realizamos el bootstrap de las interfaces de datos de los modelos
	bootstrapDatastores()

	//Realizamos las migraciones
	migrations()

}

func bootstrapDatastores() {

	//Inyectamos las dependencias de los modelos
	user.SetDatastore(UserDatastore{})
	rol.SetDatastore(RolDatastore{})
}

func migrations() {

	//AutoMigrations
	//Creamos las tablas para los modelos
	err := dataBase.AutoMigrate(
		rol.New(),
		user.New(),
	).Error
	if err != nil {
		log.Fatal(err)
	}

	dataBase.Model(user.New()).AddForeignKey("rol_id", "roles(id)", "RESTRICT", "RESTRICT")

	//Rol
	//Creamos el rol por defecto admin
	r := rol.Model{
		Name:   "admin",
		Active: true,
	}

	//Verificamos que el rol no este creado
	err = r.GetByName()
	if err != nil {

		//Si no está creado lo creamos
		err = r.Create()
		if err != nil {
			log.Panic(err)
		}
	}

	//Admin
	//Creamos el usuario por defecto admin
	admin := user.Model{
		UserName: "admin",
		FullName: "Admin",
		Email:    "admin@admin.com",
		Password: "123456",
		Rol:      r,
	}

	//Verificamos que el usuario no este creado
	err = admin.GetByUserName()
	if err != nil {

		//Si no está creado lo creamos
		err = admin.Create()
		if err != nil {
			log.Fatal(err)
		}
	}

}
