package bootstrapper

import (
	"gitlab.com/golanges/cms-es/configuration"
	"gitlab.com/golanges/cms-es/data"
)

//StartUp inicializa la app.
func StartUp() {

	//Inicializa la variable AppConfig del paquete configuration.
	configuration.LoadAppConfig()

	//Inicializa el paquete data.
	data.InitDB(configuration.AppConfig)

}
