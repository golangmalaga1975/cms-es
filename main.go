package main

import (
	"log"

	"github.com/asaskevich/govalidator"
	"gitlab.com/golanges/cms-es/bootstrapper"
	"gitlab.com/golanges/cms-es/configuration"
	"gitlab.com/golanges/cms-es/data"
	"gitlab.com/golanges/cms-es/router"
)

func init() {
	//Especificamos que lo validación sea requerida por default.
	govalidator.SetFieldsRequiredByDefault(true)
}

func main() {

	//Inicializa la app.
	bootstrapper.StartUp()

	//Aseguramos el cierre de la conexión a la base de datos.
	defer data.Close()

	//Inicializa el router.
	e := router.InitRouter()

	//Si estamos en desarrollo establecemos el modo Debug de echo a true.
	if configuration.AppConfig.Development {
		e.Debug = true
	}

	//Creamos nuestro servidor.
	log.Println("Listening in:", configuration.AppConfig.Server)
	e.Logger.Fatal(e.Start(configuration.AppConfig.Server))

}
